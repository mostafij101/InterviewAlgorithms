package array;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RotateArrayTest {

    RotateArray rotateArray;

    @Before
    public void init() {
        rotateArray = new RotateArray();
    }

    @Test
    public void rotateTest() {
        int nums[] = {1,2,3,4,5,6,7};
        int result[] = {5,6,7,1,2,3,4};

        rotateArray.rotate(nums, 3);

        assertArrayEquals(result, nums);
    }

    @Test
    public void bubbleRotateTest() {
        int nums[] = {1,2,3,4,5,6,7};
        int result[] = {5,6,7,1,2,3,4};

        rotateArray.bubbleRotate(nums, 3);

        assertArrayEquals(result, nums);
    }

    @Test
    public void reverseRotateTest() {
        int nums[] = {1,2,3,4,5,6,7};
        int result[] = {5,6,7,1,2,3,4};

        rotateArray.reversalRotate(nums, 3);

        assertArrayEquals(result, nums);
    }

    @After
    public void dinit() {
        rotateArray = null;
    }

}